FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build-env
ENV DOTNET_CLI_TELEMETRY_OPTOUT 1
WORKDIR /app



# Copy csproj and restore as distinct layers
COPY *.csproj ./
RUN dotnet restore

# Copy everything else and build
COPY . ./
RUN dotnet publish --no-restore -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:6.0
EXPOSE 80
# EXPOSE 443
# EXPOSE 7087
# EXPOSE 5087
COPY --from=build-env /app/out .
ENV ASPNETCORE_ENVIRONMENT "Development"
ENTRYPOINT ["./WebApplication3"]
